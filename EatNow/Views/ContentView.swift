//
//  ContentView.swift of project named EatNow
//
//  Created by Aurore D (@Tuwleep) on 01/06/2023.
//
//  

import SwiftUI

struct ContentView: View {
    
    // Toggle
    @State var isMenu: Bool = true
    @State var isKetchup:Bool = false
    @State var isMustard: Bool = false
    @State var isLargeFries: Bool = false
    
    // Slider
    @State var cheeseSlider: Double = 50.0
    
    //Stepper
    @State var baconStepper: Int = 0
    @State var pickleStepper: Int = 0
    
    //DatePicker
    @State var dateDay: Date = Date()
    
    //Picker Segmented
    @State var typeMenu = ["Boeuf", "Poulet", "Poisson", "Veggie"]
    @State var indexChoiceMenu: Int = 0
    
    @State var indexChoiceDrink: Int = 0
    @State var typeDrink = ["Soda", "Orange", "Citron", "Ice tea", "Eau"]
    
    //TextField
    @State var name = ""
    @State var phone = ""
    
    @State var isValidate = false
    
    @State var isVeggie = false
    
    var body: some View {
        Form {
            Section ("Composez votre commande"){
                ChoiceMenu(isMenu: $isMenu)
                ContentChoiceTypeMenu
                if typeMenu[indexChoiceMenu] != "Veggie" {
                    ChoiceBacon(baconStepper: $baconStepper)
                }
                ChoicePickle(pickleStepper: $pickleStepper)
                ChoiceKetchup(isKetchup: $isKetchup)
                ChoiceMustard(isMustard: $isMustard)
                ChoiceCheese(cheeseSlider: $cheeseSlider)
            }
            
            if isMenu == true {
                Section ("Pour le menu") {
                    ContentChoiceTypeDrink
                    ChoiceFries(isLargeFries: $isLargeFries)
                }
            }
            Section ("informations"){
                MyName(name: $name)
                MyContact(phone: $phone)
                MyTime(dateDay: $dateDay)
            }
            
            Section ("récapitulatif") {
                ContentValidate
                ContentSummary
            }
        }
    }
}

extension ContentView {
    
    private var ContentChoiceTypeMenu: some View {
        Group {
            HStack {
                Spacer()
                Image(typeMenu[indexChoiceMenu])
                    .resizable()
                    .scaledToFit()
                    .frame(height: 50)
                Spacer()
            }
            Picker(typeMenu[indexChoiceMenu], selection: $indexChoiceMenu) {
                ForEach(0..<typeMenu.count, id: \.self) { i in
                    Text(typeMenu[i]).tag(i)
                }
            }.pickerStyle(.segmented)
        }
    }
    
    private var ContentChoiceTypeDrink: some View {
        HStack {
            Icone(image: "soda")
            Picker(typeDrink[indexChoiceDrink], selection: $indexChoiceDrink) {
                ForEach(0..<typeDrink.count, id: \.self) { i in
                    Text(typeDrink[i]).tag(i)
                }
            }
        }
    }
    
    private var ContentValidate: some View {
        Button("Je commande") {
            isValidate.toggle()
        }
    }
    
    private var ContentSummary: some View {
        Group {
            if isValidate == true {
                VStack(alignment: .leading) {
                    Text("\(name) a commandé pour le \(dateDay, style: .date) à \(dateDay, style: .time)")
                    if isMenu == true {
                        Text("Menu \(typeMenu[indexChoiceMenu])")
                    } else {
                        Text("Un burger simple")
                    }
                    Text("avec \(baconStepper) tranches de bacon et ")
                    Text("\(pickleStepper) rondelles de cornichons.")
                    Text("\(Int(cheeseSlider)) % de fromage")
                    
                    if isMenu == true {
                        Text("Soda : \(typeDrink[indexChoiceDrink])")
                        if isLargeFries == true {
                            Text("Grande frite")
                        } else {
                            Text("Petite frite")
                        }
                    }
                }
                .frame(height: 200)
                .environment(\.locale, Locale(identifier: "fr"))
            } else {
                Spacer()
                    .frame(height: 200)
            }
        }
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
