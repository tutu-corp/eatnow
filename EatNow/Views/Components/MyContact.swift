//
//  MyContact.swift of project named EatNow
//
//  Created by Aurore D (@Tuwleep) on 01/06/2023.
//
//  

import SwiftUI

struct MyContact: View {
    
    @Binding var phone: String
    @FocusState var focusedPhone: Bool
    
    var body: some View {
        HStack {
            TextField("Contact", text: $phone)
                .keyboardType(.phonePad)
                .focused($focusedPhone)
            Button("Ok") {
                focusedPhone = false
            }
        }
    }
}

struct MyContact_Previews: PreviewProvider {
    static var previews: some View {
        MyContact(phone: .constant("0389438230"))
    }
}
