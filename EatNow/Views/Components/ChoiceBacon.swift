//
//  ChoiceBacon.swift of project named EatNow
//
//  Created by Aurore D (@Tuwleep) on 01/06/2023.
//
//  

import SwiftUI

struct ChoiceBacon: View {
    
    @Binding var baconStepper: Int
    
    var body: some View {
        HStack {
            Icone(image: "bacon")
            Stepper("Nombre de tranches : \(baconStepper)", value: $baconStepper, in: 0...5)
        }
    }
}

struct ChoiceBacon_Previews: PreviewProvider {
    static var previews: some View {
        ChoiceBacon(baconStepper: .constant(2))
    }
}
