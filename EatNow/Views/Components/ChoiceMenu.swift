//
//  ChoiceMenu.swift of project named EatNow
//
//  Created by Aurore D (@Tuwleep) on 01/06/2023.
//
//  

import SwiftUI

struct ChoiceMenu: View {
    
    @Binding var isMenu: Bool
    
    var body: some View {
        Toggle(isMenu ? "Menu": "Burger Simple", isOn: $isMenu)
    }
}

struct ChoiceMenu_Previews: PreviewProvider {
    static var previews: some View {
        ChoiceMenu(isMenu: .constant(true))
    }
}
