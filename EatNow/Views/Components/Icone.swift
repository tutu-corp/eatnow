//
//  Icone.swift of project named EatNow
//
//  Created by Aurore D (@Tuwleep) on 01/06/2023.
//
//  

import SwiftUI

struct Icone: View {
    
    var image: String
    
    var body: some View {
        Image(image)
            .resizable()
            .scaledToFit()
            .frame(height: 25)
    }
}

struct Icone_Previews: PreviewProvider {
    static var previews: some View {
        Icone(image: "gherkin")
    }
}
