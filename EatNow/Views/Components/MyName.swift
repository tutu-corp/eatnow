//
//  MyName.swift of project named EatNow
//
//  Created by Aurore D (@Tuwleep) on 01/06/2023.
//
//  

import SwiftUI

struct MyName: View {
    
    @Binding var name: String
    
    var body: some View {
        TextField("Nom de la commande", text: $name)
    }
}

struct MyName_Previews: PreviewProvider {
    static var previews: some View {
        MyName(name: .constant("Name"))
    }
}
