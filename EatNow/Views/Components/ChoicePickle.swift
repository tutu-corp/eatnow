//
//  ChoicePickle.swift of project named EatNow
//
//  Created by Aurore D (@Tuwleep) on 01/06/2023.
//
//  

import SwiftUI

struct ChoicePickle: View {
    
    @Binding var pickleStepper: Int
    
    var body: some View {
        HStack {
            Icone(image: "gherkin")
            Stepper("Nombre de rondelles : \(pickleStepper)", value: $pickleStepper, in: 0...5)
        }
    }
}

struct ChoicePickle_Previews: PreviewProvider {
    static var previews: some View {
        ChoicePickle(pickleStepper: .constant(0))
    }
}
