//
//  MyTime.swift of project named EatNow
//
//  Created by Aurore D (@Tuwleep) on 01/06/2023.
//
//  

import SwiftUI

struct MyTime: View {
    
    @Binding var dateDay: Date
    
    var body: some View {
        DatePicker("Livraison",
                   selection: $dateDay,
                   in: Date()...Date(timeIntervalSinceNow: 360000),
                   displayedComponents: .hourAndMinute)
    }
}

struct MyTime_Previews: PreviewProvider {
    static var previews: some View {
        MyTime(dateDay: .constant(Date()))
    }
}
