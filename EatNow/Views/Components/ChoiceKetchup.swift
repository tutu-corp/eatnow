//
//  ChoiceKetchup.swift of project named EatNow
//
//  Created by Aurore D (@Tuwleep) on 01/06/2023.
//
//  

import SwiftUI

struct ChoiceKetchup: View {
    
    @Binding var isKetchup: Bool
    
    var body: some View {
        HStack {
            Icone(image: "ketchup")
            Toggle("Ketchup", isOn: $isKetchup)
                .tint(.red)
        }
    }
}

struct ChoiceKetchup_Previews: PreviewProvider {
    static var previews: some View {
        ChoiceKetchup(isKetchup: .constant(true))
    }
}
