//
//  ChoiceCheese.swift of project named EatNow
//
//  Created by Aurore D (@Tuwleep) on 01/06/2023.
//
//  

import SwiftUI

struct ChoiceCheese: View {
    
    @Binding var cheeseSlider: Double
    
    var body: some View {
        VStack {
            HStack {
                Icone(image: "cheese")
                Text("\(Int(cheeseSlider)) % de bonheur en plus")
            }
            Slider(
                value: $cheeseSlider, in: 0...100
            ) {
            } minimumValueLabel: {
                Text("0%")
            } maximumValueLabel: {
                Text("100%")
            }
        }
    }
}

struct ChoiceCheese_Previews: PreviewProvider {
    static var previews: some View {
        ChoiceCheese(cheeseSlider: .constant(50))
    }
}
