//
//  ChoiceFries.swift of project named EatNow
//
//  Created by Aurore D (@Tuwleep) on 01/06/2023.
//
//  

import SwiftUI

struct ChoiceFries: View {
    
    @Binding var isLargeFries: Bool
    
    var body: some View {
        HStack {
            Icone(image: "fries")
            Toggle(isLargeFries ? "Grande": "Petite", isOn: $isLargeFries )
        }
    }
}

struct ChoiceFries_Previews: PreviewProvider {
    static var previews: some View {
        ChoiceFries(isLargeFries: .constant(true))
    }
}
