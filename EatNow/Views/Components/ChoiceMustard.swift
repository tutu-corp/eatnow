//
//  ChoiceMustard.swift of project named EatNow
//
//  Created by Aurore D (@Tuwleep) on 01/06/2023.
//
//  

import SwiftUI

struct ChoiceMustard: View {
    
    @Binding var isMustard: Bool
    
    var body: some View {
        HStack {
            Icone(image: "mustard")
            Toggle("Moutarde", isOn: $isMustard)
                .tint(.yellow)
        }
    }
}

struct ChoiceMustard_Previews: PreviewProvider {
    static var previews: some View {
        ChoiceMustard(isMustard: .constant(true))
    }
}
