//
//  EatNowApp.swift of project named EatNow
//
//  Created by Aurore D (@Tuwleep) on 01/06/2023.
//
//  

import SwiftUI

@main
struct EatNowApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
